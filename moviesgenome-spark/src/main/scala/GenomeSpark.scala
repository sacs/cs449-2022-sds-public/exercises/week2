import org.apache.spark.sql.SparkSession
import org.apache.spark.rdd._
import org.apache.log4j.Logger
import org.apache.log4j.Level
import scala.tools.nsc.io.File

object GenomeSpark {
    def main(args: Array[String]): Unit = {
        Logger.getLogger("org").setLevel(Level.ERROR)
        Logger.getLogger("akka").setLevel(Level.ERROR)

        val spark = SparkSession.builder
                                 .appName("Movie Genome").getOrCreate()
        val (genes, movies) = loadData(spark)
        val recommendations = run(genes, movies)
        writeFile(recommendations)
        spark.stop()
    }

    def run(genes: RDD[(Int, (Int, Double))],
            movies: RDD[(Int, String)]) = {
        val genByMovie = genes.groupByKey().cache()
        // Set of movies to be recommended
        val myMovies = Set(1,2,3,4,5)
        println("To recommend: " + myMovies.mkString(","))
        val k: Int = 100
        val recommendations =
            for(m <- myMovies;
                knn = knearest(m, k, genByMovie)
            ) yield Array(m) ++ knn
        recommendations.map(_.mkString(",")).toList
    }

    def knearest(movieId: Int, k: Int,
                 genome: RDD[(Int, Iterable[(Int, Double)])]): Array[Int] = {

        def summation(p: Iterable[(Int, Double)],
                      q: Iterable[(Int, Double)]) = {
            val squareOfDiff =
                for((tag: Int, relevance1) <- p;
                    (`tag`, relevance2) <- q;
                    diff = relevance2 - relevance1
                ) yield diff * diff
            squareOfDiff.sum
        }

        val query_movie_tags = genome.lookup(movieId).head

        genome
            .map{case (movie, tags) => (movie, summation(query_movie_tags, tags))}
            .filter(_._1 != movieId)
            .sortBy(_._2)
            .take(k)
            .map(_._1)

    }

    val filename = "./knnGenome-ml-25m.txt"

    def writeFile(lines: List[String]): Unit = {
        val file = File(filename).createFile()
        val bw = file.bufferedWriter(append = true)
        for (line <- lines) {
            println(line)
            bw.write(line + "\n")
        }
        bw.close()
    }

    def loadData(spark: SparkSession): 
                             (RDD[(Int, (Int, Double))], RDD[(Int, String)]) = {
        val movieLensHomeDir = "/cs449/movielens/ml-25m"
        val genome = spark.read.options(Map("header" -> "true"))
                        .csv(movieLensHomeDir + "/genome-scores.csv").rdd
                        .map(r => (r.getString(0).toInt,
                                   (r.getString(1).toInt, r.getString(2).toDouble)))
        val movies =  spark.read.format("csv").option("header", "true")
                           .load(movieLensHomeDir + "/movies.csv").rdd
                           .map(r => (r.getString(0).toInt, r.getString(1)))
        (genome, movies.cache())
    }
}
